package org.tahhan.research;

import org.apache.mahout.cf.taste.common.TasteException;
import org.apache.mahout.cf.taste.common.Weighting;
import org.apache.mahout.cf.taste.eval.RecommenderBuilder;
import org.apache.mahout.cf.taste.impl.neighborhood.NearestNUserNeighborhood;
import org.apache.mahout.cf.taste.impl.neighborhood.ThresholdUserNeighborhood;
import org.apache.mahout.cf.taste.impl.recommender.GenericUserBasedRecommender;
import org.apache.mahout.cf.taste.impl.similarity.CityBlockSimilarity;
import org.apache.mahout.cf.taste.impl.similarity.EuclideanDistanceSimilarity;
import org.apache.mahout.cf.taste.impl.similarity.PearsonCorrelationSimilarity;
import org.apache.mahout.cf.taste.impl.similarity.SpearmanCorrelationSimilarity;
import org.apache.mahout.cf.taste.impl.similarity.UncenteredCosineSimilarity;
import org.apache.mahout.cf.taste.model.DataModel;
import org.apache.mahout.cf.taste.neighborhood.UserNeighborhood;
import org.apache.mahout.cf.taste.recommender.Recommender;
import org.apache.mahout.cf.taste.recommender.UserBasedRecommender;
import org.apache.mahout.cf.taste.similarity.UserSimilarity;


public class RecommenderBuilderFactory implements RecommenderBuilder {
  final static Integer NEIGHBOURHOOD_SIZE = 10;
  final static float DEFAULT_THRESHOLD = (float) 0.2;
  DataModel dataModel;
  UserSimilarity similarity;
  UserNeighborhood neighborhood;
  UserBasedRecommender recommender;

  @Override
  public Recommender buildRecommender(DataModel dataModel) throws TasteException {
    return new GenericUserBasedRecommender(dataModel, neighborhood , similarity);
  }

  public RecommenderBuilderFactory(DataModel dataModel, int similarityImpl, int neighbourhoodImpl, Weighting weighting) throws TasteException   {
    this.dataModel = dataModel;

    switch (similarityImpl) {
      case 1:
        similarity = new PearsonCorrelationSimilarity(dataModel, weighting); //+weighting
        break;

      case 2:
        similarity = new EuclideanDistanceSimilarity(dataModel, weighting);  //+weighting
        break;

      case 3:
        similarity = new UncenteredCosineSimilarity(dataModel, weighting);  //+weighting
        break;

      case 4:
        similarity = new SpearmanCorrelationSimilarity(dataModel);
        break;

      case 5:
        similarity = new CityBlockSimilarity(dataModel);
        break;

      default:
        throw new IllegalArgumentException("Similarity should be between 1 and 5");
    }

    switch (neighbourhoodImpl) {
      case 1:
        neighborhood = new NearestNUserNeighborhood(NEIGHBOURHOOD_SIZE, similarity, dataModel);
        break;

      case 2:
        neighborhood = new NearestNUserNeighborhood(NEIGHBOURHOOD_SIZE * 2, similarity, dataModel);
        break;

      case 3:
        neighborhood = new NearestNUserNeighborhood(NEIGHBOURHOOD_SIZE * 3, similarity, dataModel);
        break;

      case 4:
        neighborhood = new NearestNUserNeighborhood(NEIGHBOURHOOD_SIZE * 5, similarity, dataModel);
        break;

      case 5:
        neighborhood = new NearestNUserNeighborhood(NEIGHBOURHOOD_SIZE * 10, similarity, dataModel);
        break;

      case 6:
        neighborhood = new ThresholdUserNeighborhood(DEFAULT_THRESHOLD, similarity, dataModel);       //20%
        break;

      case 7:
        neighborhood = new ThresholdUserNeighborhood(DEFAULT_THRESHOLD * 2, similarity, dataModel);   //40%
        break;

      case 8:
        neighborhood = new ThresholdUserNeighborhood(DEFAULT_THRESHOLD * 3, similarity, dataModel);   //60%
        break;

      case 9:
        neighborhood = new ThresholdUserNeighborhood(DEFAULT_THRESHOLD * 4, similarity, dataModel);   //80%
        break;

      case 10:
        neighborhood = new ThresholdUserNeighborhood(DEFAULT_THRESHOLD * 4.5, similarity, dataModel); //90%
        break;

      default:
        throw new IllegalArgumentException("Neighborhood should be between 1 and 10");
    }
  }

  @Override
  public String toString() {
    return "{sim:"+similarity.getClass().getSimpleName() + " + nbr:" + neighborhood.getClass().getSimpleName()+"}";
  }

}
