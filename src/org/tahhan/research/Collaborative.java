package org.tahhan.research;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.apache.mahout.cf.taste.common.TasteException;
import org.apache.mahout.cf.taste.common.Weighting;
import org.apache.mahout.cf.taste.eval.RecommenderBuilder;
import org.apache.mahout.cf.taste.eval.RecommenderEvaluator;
import org.apache.mahout.cf.taste.impl.eval.RMSRecommenderEvaluator;
import org.apache.mahout.cf.taste.impl.model.file.FileDataModel;
import org.apache.mahout.cf.taste.model.DataModel;

public class Collaborative{
  final static String RATINGS_FILE = "data/ratings.csv";
  final static float TRAINING_PERCENTAGE   = (float) 0.7;
  final static float EVALUATION_PERCENTAGE = (float) 0.3;

  //static Integer similarityFrom,similarityTo,neighbourhoodFrom,neighbourhoodTo; //shorter definition when falling back to argv

  static Integer similarityFrom = 4;
  static Integer similarityTo   = 4;
  static Integer neighbourhoodFrom = 1;
  static Integer neighbourhoodTo   = 10;

  DataModel dataModel;
  RecommenderEvaluator revaluator;
  RecommenderBuilder rbuilder;
  Integer sim;
  Integer nbr;
  Weighting weighting;

  /**
   * @param args
   * @throws Exception
   */
  public static void main(String[] args) throws Exception {
/*  Fallback to argv
    similarityFrom    = similarityFrom==null    ? Integer.parseInt(args[0]) : similarityFrom;
    similarityTo      = similarityTo==null      ? Integer.parseInt(args[1]) : similarityTo;
    neighbourhoodFrom = neighbourhoodFrom==null ? Integer.parseInt(args[2]) : neighbourhoodFrom;
    neighbourhoodTo   = neighbourhoodTo==null   ? Integer.parseInt(args[3]) : neighbourhoodTo;
*/
	DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
    Map<Integer, Integer[]> exactList =  new HashMap<Integer, Integer[]>();
    exactList.put(1, new Integer[]{1,2,3,4,5,6,7,8,9,10});
    exactList.put(2, new Integer[]{1,2,3,4,5,6,7,8,9,10});
    exactList.put(3, new Integer[]{1,2,3,4,5,6,7,8,9,10});
    for (Integer sim : exactList.keySet()) {
      for(Integer nbr : exactList.get(sim)){
        System.out.println("Start evaluating with the Following Parameters: " + sim+"/"+nbr + " At: " + dateFormat.format(new Date()));
        long startTime = System.currentTimeMillis();
        Collaborative me = new Collaborative(RATINGS_FILE, sim, nbr, Weighting.WEIGHTED);
        double result = me.evaluate();
        float duration = (System.currentTimeMillis() - startTime) / 1000;
        System.out.println(sim+"/"+nbr+" -- " +me.rbuilder.toString() + " -- result: "+ result + " -- Durations: "+duration+"sec");
      }
    }

    /*
    for (int sim = similarityFrom; sim <= similarityTo; sim++) {
      for(int nbr = neighbourhoodFrom; nbr <= neighbourhoodTo; nbr++){
        long startTime = System.currentTimeMillis();
        Collaborative me = new Collaborative(RATINGS_FILE, sim, nbr);
        double result = me.evaluate();
        float duration = (System.currentTimeMillis() - startTime) / 1000;
        System.out.println(sim+"/"+nbr+" -- " +me.rbuilder.toString() + " -- result: "+ result + " -- Durations: "+duration+"sec");
      }
    }
    */
  }

  public Collaborative(String filename, Integer similarity, Integer neighbourhood, Weighting weight) throws TasteException, IOException {
    File inputFile = new File(RATINGS_FILE);
    sim = similarity;
    nbr = neighbourhood;
    weighting = weight;
    
    dataModel  = new FileDataModel(inputFile);
    rbuilder   = new RecommenderBuilderFactory(dataModel, sim, nbr, weighting);
    revaluator = new RMSRecommenderEvaluator();
  }

  public double evaluate() throws TasteException{
    return revaluator.evaluate(rbuilder, null, dataModel, TRAINING_PERCENTAGE, EVALUATION_PERCENTAGE);
  }
}